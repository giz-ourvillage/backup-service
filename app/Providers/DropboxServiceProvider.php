<?php

namespace App\Providers;


use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use Spatie\Dropbox\Client as DropboxClient;
use Spatie\FlysystemDropbox\DropboxAdapter;
use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;

class DropboxServiceProvider extends ServiceProvider {
    /**
     * Register services.
     */
    public function register(): void
    {
        
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        Storage::extend( 'dropbox', function($app, $config )
        {
            $resource = ( new Client() )->post( $config['token_url'] , [
                    'form_params' => [ 
                            'grant_type' => 'refresh_token', 
                            'refresh_token' => $config['refresh_token'] 
                    ] 
            ]);

            $accessToken = json_decode( $resource->getBody(), true )['access_token'];

            $adapter = new DropboxAdapter(new DropboxClient($accessToken));

            return new FilesystemAdapter(new Filesystem($adapter, $config), $adapter, $config);
        });
    }
}
