# GIZ BACKUP

A service used to backup the blockchain and database

# Running 

Make sure you have PHP version 8 at least install

Then run 

`composer install`

Once done 

`php artisan backup:run`

You can update the settings of the backup by editing the file `config/backup.php`

# Generating refresh token and access token

Follow the details listed here for obtaining the keys [https://github.com/spatie/flysystem-dropbox/issues/95](https://github.com/spatie/flysystem-dropbox/issues/95)

# Storage 

The backup are sent to dropbox at the moment. You can use another type of storage system by updating the 
file `config/filesystems.php`